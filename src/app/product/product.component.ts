import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Pallier, Product } from '../world';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RestserviceService } from '../restservice.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  private _product: Product = new Product();
  private _qtmulti: string = "1";
  private _money: number = 0;
  private _server: string = "";
  progressBarValue: number = 0;
  lastUpdate = 0;
  moneyGoal: number = 0;
  nbCanBeBought: number = 0;
  isDisabled: boolean = true;
  constructor(
    private snackBar: MatSnackBar,
    private service: RestserviceService
  ) { }

  ngOnInit(): void {
    setInterval(() => { this.calculerScore(); this.calculerMaxCanBeBought(); }, 100);
  }

  //Getters
  get product() {
    return this._product;
  }

  get server() {
    return this._server;
  }

  @Input()
  set server(server: string) {
    this._server = server;
  }

  @Input()
  set prod(value: Product) {
    this._product = value;
    if (this._product && this._product.timeleft > 0) {
      this.lastUpdate = Date.now();
      this.progressBarValue = (this._product.vitesse - this._product.timeleft) / this._product.vitesse;
    }
  }

  @Input()
  set qtmulti(value: string) {
    this._qtmulti = value;
    if (this._qtmulti && this._product) this.calculerMaxCanBeBought();
  }

  @Input()
  set money(value: number) {
    this._money = value;
  }

  @Output() notifyProduction: EventEmitter<Product> =
    new EventEmitter<Product>();

  @Output() notifyBuy: EventEmitter<number> = new EventEmitter<number>();

  startFabrication() {
    if (this._product.timeleft === 0 && this._product.quantite > 0) {
      this._product.timeleft = this._product.vitesse;
      this.lastUpdate = Date.now();
      if (!this._product.managerUnlocked) {
        this.service.putProduct(this.product);
      }
    }
  }

  acheterProduit() {
    this._product.quantite += this.nbCanBeBought;
    this._product.cout = this._product.cout * Math.pow(this._product.croissance, this.nbCanBeBought);
    for (var unlock of this._product.palliers.pallier) {
      if (this._product.quantite >= unlock.seuil && !unlock.unlocked) {
        unlock.unlocked = true;
        this.popMessage(unlock.name + ' ' + this._product.name + ' ' + unlock.typeratio + ' x' + unlock.ratio);
        this.upgrade(unlock);
      }
    }
    this.service.putProduct(this._product);
    this.notifyBuy.emit(this.moneyGoal);
  }

  upgrade(pallier: Pallier) {
    switch (pallier.typeratio) {
      case 'gain':
        this._product.revenu = this._product.revenu * pallier.ratio;
        break;
      case 'vitesse':
        this._product.timeleft = this._product.timeleft / pallier.ratio;
        this._product.vitesse = this._product.vitesse / pallier.ratio;
        break;
      case 'quantite':
        this._product.quantite += pallier.ratio;
        break;
      default:
        break;
    }
  }

  // Méthodes privées 

  private popMessage(message: string): void {
    this.snackBar.open(message, '', { duration: 2000 });
  }

  private calculerMaxCanBeBought() {
    this.moneyGoal = (this._product.cout * (1 - Math.pow(this._product.croissance, this.nbCanBeBought))) /
          (1 - this._product.croissance);
    switch (this._qtmulti) {
      case 'x1':
        this.nbCanBeBought = 1;
        break;
      case 'x10':
        this.nbCanBeBought = 10;
        break;
      case 'x100':
        this.nbCanBeBought = 100;
        break;
      case 'xMax':
        this.nbCanBeBought = Math.trunc(Math.log(1 -(this._money * (1 - this._product.croissance)) /
            this._product.cout) / Math.log(this._product.croissance)
        );
        break;
      default:
        break;
    }
    this.isDisabled = this._money <= this.moneyGoal ? true : false;
  }

  private calculerScore() {
    if ((this._product.managerUnlocked && this._product.quantite > 0) || this._product.timeleft !== 0) {
      this._product.timeleft -= Date.now() - this.lastUpdate;
      this.lastUpdate = Date.now();
      if (this._product.timeleft <= 0) {
        this.progressBarValue = 0;
        this.notifyProduction.emit(this._product);
        this._product.timeleft = this._product.managerUnlocked ? this._product.vitesse : 0;
      } else {
        this.progressBarValue = ((this._product.vitesse - this._product.timeleft) / this._product.vitesse) * 100;
      }
    }
  }

}