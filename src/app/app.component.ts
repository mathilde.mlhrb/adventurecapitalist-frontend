import { stringify } from '@angular/compiler/src/util';
import { Component, QueryList, ViewChildren } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProductComponent } from './product/product.component';
import { RestserviceService } from './restservice.service';
import { World, Product, Pallier } from './world';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})
export class AppComponent {

  title = 'ADVENTURE-CAPITALIST';
  world: World = new World();
  server: string;
  username: string = "";
  showManagers: Boolean = false;
  showInvestors: Boolean = false;
  showCashUpgrades: Boolean = false;
  showUnlocks: Boolean = false;
  showAngelUpgrades: Boolean = false;
  product: Product = new Product();
  qtmulti: string = "x1";
  qtmultiValues = ['x1', 'x10', 'x100', 'xMax'];
  badgeManagers: number = 0;
  badgeUpgrades: number = 0;
  badgeAngelUpgrades: number = 0;
  angelsToClaim: number = 0;

  @ViewChildren(ProductComponent)
  productsListComponent: QueryList<ProductComponent> = new QueryList<ProductComponent>();


  constructor(private service: RestserviceService, private snackBar: MatSnackBar) {
    service.getWorld().then((world) => {
      this.world = world;
      this.calculerAngels();
      this.calculerBadges();
    });
    this.server = service.server;
    this.username = this.valueOf(localStorage.getItem('username'));
    this.service.user = this.username;
  }

  onProductionDone(product: Product) {
    this.world.money += product.quantite * product.revenu * (1 + (this.world.activeangels * this.world.angelbonus) / 100);
    this.world.score += product.quantite * product.revenu * (1 + (this.world.activeangels * this.world.angelbonus) / 100);
    this.calculerBadges();
    this.calculerAngels();
  }

  onUsernameChanged() {
    localStorage.setItem('username', this.username);
    this.service.user = this.username;
    this.service.getWorld().then((world) => {
      this.world = world;
      this.calculerAngels();
      this.calculerBadges();
    });
  }

  changeQMulti() {
    var currentQMulti = this.qtmultiValues.indexOf(this.qtmulti);
    var index = this.qtmultiValues.length === currentQMulti + 1 ? 0 : currentQMulti + 1;
    this.qtmulti = this.qtmultiValues[index];
  }

  hireManager(manager: Pallier): void {
    if (this.world.money >= manager.seuil) {
      this.world.money -= manager.seuil;
      manager.unlocked = true;
      var produitEnquestion = this.world.products.product.filter(x => x.id == manager.idcible)[0];
      produitEnquestion.managerUnlocked = true;
      this.popMessage("Félicitations ! Tu viens d'embaucher " + manager.name + " pour t'aider à la préparation de " + produitEnquestion.name.toLocaleLowerCase());
      this.calculerBadges();
      this.service.putManager(manager);
    }
  }

  buyUpgrade(upgrade: Pallier) {
    this.world.money -= upgrade.seuil;
    this.upgrade(upgrade);
    this.service.putUpgrade(upgrade);
  }

  buyAngelUpgrade(upgrade: Pallier) {
    this.world.activeangels -= upgrade.seuil;
    this.upgrade(upgrade);
    this.service.putAngelUpgrade(upgrade);
  }

  onBuy(cost: number) {
    this.world.money -= cost;
    this.calculerBadges();
    var minQuantite = this.world.allunlocks.pallier[0].seuil;
    for (var produit of this.world.products.product) {
      if (produit.quantite < minQuantite) {
        minQuantite = produit.quantite;
      }
    }
    for (var unlock of this.world.allunlocks.pallier) {
      if (minQuantite >= unlock.seuil && !unlock.unlocked) {
        unlock.unlocked = true;
        this.productsListComponent.forEach((p) => p.upgrade(unlock));
        this.popMessage(
          unlock.name + ' ' + unlock.typeratio + ' x' + unlock.ratio
        );
      }
    }
  }

  redemarrerWorld() {
    this.service.deleteWorld();
    this.service.getWorld().then((world) => {
      this.world = world;
      this.calculerAngels();
      this.calculerBadges();
    });
  }

  // Private methods 
  private popMessage(message: string): void {
    this.snackBar.open(message, "", { duration: 2000 })
  }

  private calculerBadges() {
    this.badgeManagers = 0;
    this.badgeUpgrades = 0;
    this.badgeAngelUpgrades = 0;
    for (var manager of this.world.managers.pallier) {
      if (!manager.unlocked && this.world.money >= manager.seuil) {
        this.badgeManagers++;
      }
    }
    for (var upgrade of this.world.upgrades.pallier) {
      if (!upgrade.unlocked && this.world.money >= upgrade.seuil) {
        this.badgeUpgrades++;
      }
    }
    for (var angelUpgrade of this.world.angelupgrades.pallier) {
      if (!angelUpgrade.unlocked && this.world.money >= angelUpgrade.seuil) {
        this.badgeAngelUpgrades++;
      }
    }
  }

  private calculerAngels() {
    this.angelsToClaim =
      150 * Math.sqrt(this.world.score / Math.pow(10, 15)) -
      this.world.totalangels;
  }

  // Caste une variable de type string | null en string --> résultante du mode stricte
  private valueOf(obj: string | null): string {
    return typeof obj == 'string' ? obj.valueOf() : "";
  }

  private upgrade(upgrade: Pallier) {
    upgrade.unlocked = true;
    var message = upgrade.name + ' ';
    switch (upgrade.idcible) {
      case -1:
        this.world.angelbonus += upgrade.ratio;
        message += '+' + upgrade.ratio + '%';
        break;
      case 0:
        message += 'tous les profits x' + upgrade.ratio;
        this.productsListComponent.forEach((p) => p.upgrade(upgrade));
        break;
      default:
        this.productsListComponent.forEach((p) => {
          if (upgrade.idcible === p.product.id) {
            p.upgrade(upgrade);
            message += p.product.name + ' ';
            if (upgrade.typeratio === 'quantite') {
              message += 'quantité +' + upgrade.ratio;
            } else {
              message += upgrade.typeratio + ' x' + upgrade.ratio;
            }
          }
        });
        break;
    }
    this.popMessage(message);
    this.calculerBadges();
  }
}